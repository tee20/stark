module.exports = function (sequelize, DataTypes) {

    return sequelize.define('ResetToken', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false
        }, expires: {
            type: DataTypes.DATE
        }
    },
        {
            classMethods: {},
            tableName: "reset_tokens",
            // schema: "stark",
            freezeTableName: true,
            underscored: true,
            timestamps: true
        });
};