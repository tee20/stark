'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require('config');
var db        = {};

var sequelize = new Sequelize(config.db.db, config.db.user, config.db.password, config);

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.ResetToken.belongsTo(db.User);
db.StylePreferences.belongsTo(db.User);
db.User.hasMany(db.StylePreferences);
db.Order.belongsTo(db.User);
db.User.hasMany(db.Order);
db.LineItem.belongsTo(db.Order);
db.Order.hasMany(db.LineItem);
db.Address.belongsTo(db.User);
db.User.hasMany(db.Address);
module.exports = db;
