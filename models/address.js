module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Address', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false
        },
        pincode : {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
        {
            classMethods: {},
            tableName: "addresses",
            // schema: "stark",
            freezeTableName: true,
            underscored: true,
            timestamps: true
        });
};