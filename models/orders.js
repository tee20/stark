module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Order', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        order_total: {
            type: DataTypes.DECIMAL(10, 2)
        },
        order_status: {
            type:DataTypes.STRING,
            allowNull: false
        }
    },
        {
            classMethods: {},
            tableName: "orders",
            // schema: "stark",
            freezeTableName: true,
            underscored: true,
            timestamps: true
        });
};