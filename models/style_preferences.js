module.exports = function (sequelize, DataTypes) {

    return sequelize.define('StylePreferences', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
        },
        preferences: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
        {
            classMethods: {},
            tableName: "style_preferences",
            // schema: "stark",
            freezeTableName: true,
            underscored: true,
            timestamps: true
        });
};