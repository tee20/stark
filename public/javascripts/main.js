angular.module('StarkApp', ['ngMaterial', 'ngMessages']);
angular.module('StarkApp').controller('HomeController', ['$scope', function ($scope) {
    $scope.greeting = 'Hola!';
}]);

angular.module('StarkApp').controller('LoginController', ['$scope', '$location', '$http', 'StarkService', function ($scope, $location, $http, starkService) {
    $scope.state = null;
    $scope.widget = "LOGIN";

    $scope.loginData = {};
    $scope.forgotData = {};
    $scope.resetData = {};

    $scope.reset = $location.search().reset === true;

    $scope.login = function (data) {
        $scope.$error = null;
        $scope.state = "LOGGING_IN";
        starkService.login($scope.loginData).success(function (data) {
            $location.path("/");
        }).error(function (data, status) {
            $scope.state = 'ERROR';
            $scope.$error = "Incorrect username/password";
        });
    };

    $scope.forgotPassword1 = function (data) {
        $scope.state = "FORGOT";
        starkService.forgotPassword($scope.forgotData).success(function (data) {
            $scope.widget = "FORGOT_SUCCESS";
        }).error(function (data, status) {
            console.log(data);
        });
    };

    $scope.resetPassword1 = function () {
        $scope.state = "RESETTING";
        $http.post(window.location.pathname, $scope.resetData).success(function (data) {
            window.location = "/login?reset=1";
        }).error(function (data, status) {
            console.log(data + status);
        });
    }
}]);

angular.module('StarkApp').controller('AccountController', ['$scope', 'StarkService', '$location', function ($scope, StarkService, $location) {
}]);

angular.module('StarkApp').controller('RegisterController', ['$scope', 'StarkService','$location', function ($scope, StarkService, $location) {
    $scope.registerData = {};
    $scope.state = "";
    $scope.errors = [];

    $scope.register = function () {
        $scope.state = "REGISTERING";
        if(!$scope.registerForm.$valid) {
            return;
        }
        StarkService.register($scope.registerData).success(function (data) {
           window.location = "/";
        }).error(function (errors, status) {
            angular.forEach(errors, function (error) {
                if (error.message == "username must be unique") {
                    $scope.registerForm.username.$setValidity("dupeEmail", false);
                }
            });
            $scope.state = "";
        });
    }
}]);

angular.module('StarkApp').controller('PrefsController', ['$scope', 'StarkService', function ($scope, StarkService) {
    $scope.lifestyle = null;
    $scope.body = null;

    $scope.submitPrefs = function () {
        if (!$scope.preferencesForm.$valid) {
            return;
        }
        var prefs = {
            lifestyle: $scope.lifestyle,
            body: $scope.body,
            shirtSize: $scope.shirtSize,
            pantSize: $scope.pantSize,
            budget: $scope.budget
        }
        console.log(prefs);
    }
}]);

angular.module('StarkApp').service('StarkService', ['$http', function ($http) {

    this.login = function (loginData) {
        return $http.post('users/login', loginData);
    }

    this.forgotPassword = function (forgotPassword) {
        return $http.post('users/forgot', forgotPassword);
    }

    this.resetPassword = function (resetPassword) {
        return $http.post("users/reset", resetPassword);
    }
    this.logout = function () {

    }
    this.register = function (registerData) {
        return $http.post('users', registerData);
    }

}]);

//TODO move this to it's own file'
/* detect touch */
if ("ontouchstart" in window) {
    document.documentElement.className = document.documentElement.className + " touch";
}
if (!$("html").hasClass("touch")) {
    /* background fix */
    $(".parallax").css("background-attachment", "fixed");
}

/* fix vertical when not overflow
call fullscreenFix() if .fullscreen content changes */
function fullscreenFix() {
    var h = $('body').height();
    // set .fullscreen height
    $(".content-b").each(function (i) {
        if ($(this).innerHeight() > h) {
            $(this).closest(".fullscreen").addClass("overflow");
        }
    });
}
$(window).resize(fullscreenFix);
fullscreenFix();

/* resize background images */
function backgroundResize() {
    var windowH = $(window).height();
    $(".background").each(function (i) {
        var path = $(this);
        // variables
        var contW = path.width();
        var contH = path.height();
        var imgW = path.attr("data-img-width");
        var imgH = path.attr("data-img-height");
        var ratio = imgW / imgH;
        // overflowing difference
        var diff = parseFloat(path.attr("data-diff"));
        diff = diff ? diff : 0;
        // remaining height to have fullscreen image only on parallax
        var remainingH = 0;
        if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
            var maxH = contH > windowH ? contH : windowH;
            remainingH = windowH - contH;
        }
        // set img values depending on cont
        imgH = contH + remainingH + diff;
        imgW = imgH * ratio;
        // fix when too large
        if (contW > imgW) {
            imgW = contW;
            imgH = imgW / ratio;
        }
        //
        path.data("resized-imgW", imgW);
        path.data("resized-imgH", imgH);
        path.css("background-size", imgW + "px " + imgH + "px");
    });
}
$(window).resize(backgroundResize);
$(window).focus(backgroundResize);
backgroundResize();

/* set parallax background-position */
function parallaxPosition(e) {
    var heightWindow = $(window).height();
    var topWindow = $(window).scrollTop();
    var bottomWindow = topWindow + heightWindow;
    var currentWindow = (topWindow + bottomWindow) / 2;
    $(".parallax").each(function (i) {
        var path = $(this);
        var height = path.height();
        var top = path.offset().top;
        var bottom = top + height;
        // only when in range
        if (bottomWindow > top && topWindow < bottom) {
            var imgW = path.data("resized-imgW");
            var imgH = path.data("resized-imgH");
            // min when image touch top of window
            var min = 0;
            // max when image touch bottom of window
            var max = - imgH + heightWindow;
            // overflow changes parallax
            var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
            top = top - overflowH;
            bottom = bottom + overflowH;
            // value with linear interpolation
            var value = min + (max - min) * (currentWindow - top) / (bottom - top);
            // set background-position
            var orizontalPosition = path.attr("data-oriz-pos");
            orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
            $(this).css("background-position", orizontalPosition + " " + value + "px");
        }
    });
}
if (!$("html").hasClass("touch")) {
    $(window).resize(parallaxPosition);
    //$(window).focus(parallaxPosition);
    $(window).scroll(parallaxPosition);
    parallaxPosition();
}