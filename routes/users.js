var express = require('express');
var router = express.Router();
var passport = require('passport');
var Models = require('../models');
var bcrypt = require('bcrypt');
var async = require('async');
var crypto = require('crypto');
var userUtils = require('../lib/user_utils');

var mail = require('../lib/email')


var findUser = function (req, res, next) {
  userUtils.getUserById(req.session.passport.user).then(function (user) {
    res.send({
      username: user.username,
      id: user.id
    });
  });
};

/* GET users listing. */
router.get('/', function (req, res, next) {
  findUser(req, res, next);
});



router.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) {
      res.send(500, 'Ups. Something broke!');
    } else if (info) {
      res.send(401, 'unauthorized');
    } else {
      req.login(user, function (err) {
        if (err) {
          res.send(500, 'Ups.');
        } else {
          res.send(200, JSON.stringify(user));
        }
      })
    }
  })(req, res, next);
});

router.post('/', function (req, res, next) {

  if (req.body.username == null || req.body.password == null) {
    return res.status(400).send("Username and password are required");
  }
  var salt = bcrypt.genSaltSync(10);
  var newUser = {
    username: req.body.username,
    salt: salt,
    password: bcrypt.hashSync(req.body.password, salt)
  };

  async.waterfall([
    function (callback) {
      Models.User.create(newUser).then(function (user) {
        callback(null, user);
        return null;
      }).catch(function (error) {
        return callback(error.errors, null);
      });
    },
    function (user, callback) {
      var address = {
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        pincode: req.body.pincode,
        user_id: user.id
      }
      Models.Address.create(address).then(function (address) {
        callback(null, 'done');
        return null;
      }).catch(function (error) {
        return callback(error, null);
      });
    }
  ], function (error) {
    if (error) {
      console.log(error);
     return next(error);
    }
  });
});

router.post('/forgot', function (req, res, next) {
  async.waterfall([
    function (done) {
      Models.User.findOne({
        where: {
          'username': req.body.email
        }
      }).then(function (user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot');
        }

        var token = crypto.randomBytes(20).toString('hex');
        var resetToken = Models.ResetToken.build({
          token: token,
          expires: Date.now() + 3600000, // 1 hour
          user_id: user.id
        });

        resetToken.save().then(function () {
          done(null, token, user);
        }).catch(function (error) {
          done(error, null, null);
        });
      });
    },
    function (token, user, done) {
      mail(user, req.headers.host, token, function (err) {
        done(null, 'done');
      });
    }
  ], function (err) {
    if (err) return next(err);
    res.send("");
  });
});

router.get('/reset/:token', function (req, res) {
  Models.ResetToken.findOne({
    where: {
      token: req.params.token, expires: { $gt: Date.now() }
    }
  }).then(function (token) {
    if (!token) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('reset');
  });
});

router.post('/reset/:token', function (req, res) {

  Models.ResetToken.findOne({
    where: {
      token: req.params.token, expires: { $gt: Date.now() }
    }
  }).then(function (token) {
    if (!token) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('back');
    }
    token.getUser().then(function (user) {
      var salt = bcrypt.genSaltSync(10);
      user.salt = salt;
      user.password = bcrypt.hashSync(req.body.newPassword, salt)
      user.save().then(function (user) {
        // TODO: delete token after
        res.send();
      }).catch(function (err) {
        console.log('Error');
      });
    })
  });
})

router.post('/:id/prefs', function (req, res, next) {
  var prefs = {
    preferences: JSON.stringify(req.params),
    user_id: req.params.id
  }
  Models.StylePreferences.create(prefs).then(function (prefs) {
    res.send(JSON.parse(prefs.preferences));
  });
});

router.put('/:id/prefs', function (req, res, next) {

  Models.StylePreferences.findOne({
    where: { user_id: req.session.passport.user }
  }).then(function (prefs) {
    prefs.updateAttributes({
      preferences: JSON.stringify(req.params)
    }).then(function (data) {
      res.send(data.preferences);
    });
  });
});

router.get('/:id/prefs/', function (req, res, next) {

});



module.exports = router;
