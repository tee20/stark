var express = require('express');
var router = express.Router();
var jade = require('jade');
var Model = require('../models');
var userUtils = require('../lib/user_utils');

/* GET home page. */
router.get('/', function (req, res, next) {
  // res.sendFile('index.html');
  res.render('index', { title: 'Express' });
});


router.get('/login', function (req, res, next) {
  res.render('login');
});

router.get('/faq', function (req, res, next) {
  res.render("faq");
})

router.get('/contact', function (req, res, next) {
  res.render('contact');
})

router.get('/shop-style', function (req, res, next) {
  res.render('shop-style');
});

router.get('/account', function (req, res, next) {
  userUtils.getUserById(req.session.passport.user).then(function (user) {
    Model.Order.findAll({
      where: { user_id: user.id }
    }).then(function (orders) {
      res.render('account', {
        username: user.username,
        orders: orders
      });
    })
  });
});

router.get('/register', function (req, res, next) {
  res.render('register');
});

module.exports = router;
