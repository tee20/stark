var express = require('express');
var router = express.Router();
var passport = require('passport');
var Models = require('../models');
var bcrypt = require('bcrypt');
var async = require('async');
var crypto = require('crypto');
var userUtils = require('../lib/user_utils');

var mail = require('../lib/email')

router.get('/:id', function (req, res, next) {
    Models.Order.findOne({
        where: { id: req.params.id }
    }).then(function(order){
            res.render('order', {
            order: order
        });
    });
});

module.exports = router;