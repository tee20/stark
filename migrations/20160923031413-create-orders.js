'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable('orders',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        user_id: {
          type: Sequelize.INTEGER,
          references: {
            model: "users",
            key: "id"
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        created_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updated_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        order_total: {
          type: Sequelize.DECIMAL(10, 2)
        },
        order_status: {
          type: Sequelize.STRING,
          allowNull: false
        }
      });
    return queryInterface.createTable('line_items',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        order_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: "orders",
            key: "id"
          }
        },
        description: {
          type: Sequelize.STRING,
          allowNull: false
        },
        size: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updated_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        price: {
          type: Sequelize.DECIMAL(10, 2),
          allowNull: false
        }
      });
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.dropTable("orders");
    return queryInterface.dropTable("line_items");
  }
};
