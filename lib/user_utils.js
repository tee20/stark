var Models = require('../models');

module.exports = {
    getUserById: function (id) {
        return Models.User.findOne({
            where: { 'id': id }
        });
    }
}
