var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

var sendmail = function (user, host, token, callback) {
    var mailer = nodemailer.createTransport(sgTransport({
        auth: {
            api_key: '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'
        }
    }));
    var mailOptions = {
        to: user.username,
        from: 'passwordreset@tee20.com',
        subject: 'Tee20 Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        'http://' + host + 'users/reset/' + token + '\n\n' +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    };
    mailer.sendMail(mailOptions, function (err) {
        callback(err);
    });

};

module.exports = sendmail;